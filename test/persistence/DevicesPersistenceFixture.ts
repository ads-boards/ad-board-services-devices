let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { FilterParams, MultiString } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';

import { DeviceV1 } from '../../src/data/version1/DeviceV1';

import { IDevicesPersistence } from '../../src/persistence/IDevicesPersistence';

let DEVICE1: DeviceV1 = {
    id: '1',
    request_code: 'id123',
    description: 'Device 1',
    last_connection: new Date()
};
let DEVICE2: DeviceV1 = {
    id: '2',
    request_code: 'id321',
    description: 'Device 2',
    last_connection: new Date()
};

let DEVICE3: DeviceV1 = {
    id: '3',
    request_code: '',
    description: 'Device 3',
    last_connection: new Date()
};

export class DevicesPersistenceFixture {
    private _persistence: IDevicesPersistence;
    
    constructor(persistence) {
        assert.isNotNull(persistence);
        this._persistence = persistence;
    }

    private testCreateDevices(done) {
        async.series([
        // Create one device
            (callback) => {
                this._persistence.create(
                    null,
                    DEVICE1,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.id, DEVICE1.id);
                        assert.equal(device.request_code, DEVICE1.request_code);
                        assert.equal(device.description, DEVICE1.description);

                        callback();
                    }
                );
            },
        // Create another device
            (callback) => {
                this._persistence.create(
                    null,
                    DEVICE2,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.id, DEVICE2.id);
                        assert.equal(device.request_code, DEVICE2.request_code);
                        assert.equal(device.description, DEVICE2.description);

                        callback();
                    }
                );
            },
        // Create yet another device
            (callback) => {
                this._persistence.create(
                    null,
                    DEVICE3,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.id, DEVICE3.id);
                        assert.equal(device.request_code, DEVICE3.request_code);
                        assert.equal(device.description, DEVICE3.description);

                        callback();
                    }
                );
            }
        ], done);
    }
                
    testCrudOperations(done) {
        let device1: DeviceV1;

        async.series([
        // Create items
            (callback) => {
                this.testCreateDevices(callback);
            },
        // Get all devices
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    new FilterParams(),
                    new PagingParams(),
                    (err, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 3);

                        device1 = page.data[0];

                        callback();
                    }
                );
            },
        // Update the device
            (callback) => {
                device1.description =  'Updated Text 1';

                this._persistence.update(
                    null,
                    device1,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.description, 'Updated Text 1');
                        assert.equal(device.id, device1.id);

                        callback();
                    }
                );
            },
        // Delete device
            (callback) => {
                this._persistence.deleteById(
                    null,
                    device1.id,
                    (err) => {
                        assert.isNull(err);

                        callback();
                    }
                );
            },
        // Try to get delete device
            (callback) => {
                this._persistence.getOneById(
                    null,
                    device1.id,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isNull(device || null);

                        callback();
                    }
                );
            }
        ], done);
    }

    testGetWithFilter(done) {
        async.series([
        // Create devices
            (callback) => {
                this.testCreateDevices(callback);
            },
        // Get devices filtered by id
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        id: '1'
                    }),
                    new PagingParams(),
                    (err, devices) => {
                        assert.isNull(err);

                        assert.isObject(devices);
                        assert.lengthOf(devices.data, 1);

                        callback();
                    }
                );
            },
        // Get devices filtered by search
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        search: 'id321'
                    }),
                    new PagingParams(),
                    (err, devices) => {
                        assert.isNull(err);

                        assert.isObject(devices);
                        assert.lengthOf(devices.data, 1);

                        callback();
                    }
                );
            }
        ], done);
    }

}
