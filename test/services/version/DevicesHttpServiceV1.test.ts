let _ = require('lodash');
let async = require('async');
let restify = require('restify');
let assert = require('chai').assert;

import { ConfigParams, MultiString } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';

import { DeviceV1 } from '../../../src/data/version1/DeviceV1';
import { DevicesMemoryPersistence } from '../../../src/persistence/DevicesMemoryPersistence';
import { DevicesController } from '../../../src/logic/DevicesController';
import { DevicesHttpServiceV1 } from '../../../src/services/version1/DevicesHttpServiceV1';

let httpConfig = ConfigParams.fromTuples(
    "connection.protocol", "http",
    "connection.host", "localhost",
    "connection.port", 3000
);

let DEVICE1: DeviceV1 = {
    id: '1',
    request_code: 'id123',
    description: 'Device 1',
    last_connection: new Date()
};
let DEVICE2: DeviceV1 = {
    id: '2',
    request_code: '',
    description: 'Device 2',
    last_connection: new Date()
};

suite('DevicesHttpServiceV1', ()=> {    
    let service: DevicesHttpServiceV1;
    let rest: any;

    suiteSetup((done) => {
        let persistence = new DevicesMemoryPersistence();
        let controller = new DevicesController();

        service = new DevicesHttpServiceV1();
        service.configure(httpConfig);

        let references: References = References.fromTuples(
            new Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '1.0'), persistence,
            new Descriptor('ad-board-devices', 'controller', 'default', 'default', '1.0'), controller,
            new Descriptor('ad-board-devices', 'service', 'http', 'default', '1.0'), service
        );
        controller.setReferences(references);
        service.setReferences(references);

        service.open(null, done);
    });
    
    suiteTeardown((done) => {
        service.close(null, done);
    });

    setup(() => {
        let url = 'http://localhost:3000';
        rest = restify.createJsonClient({ url: url, version: '*' });
    });
    
    
    test('CRUD Operations', (done) => {
        let device1, device2;

        async.series([
        // Create one device
            (callback) => {
                rest.post('/v1/devices/create_device',
                    {
                        device: DEVICE1
                    },
                    (err, req, res, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.id, DEVICE1.id);
                        assert.equal(device.request_code, DEVICE1.request_code);
                        assert.equal(device.description, DEVICE1.description);

                        device1 = device;

                        callback();
                    }
                );
            },
        // Create another device
            (callback) => {
                rest.post('/v1/devices/create_device', 
                    {
                        device: DEVICE2
                    },
                    (err, req, res, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.id, DEVICE2.id);
                        assert.isNotNull(device.request_code);
                        assert.equal(device.description, DEVICE2.description);

                        device2 = device;

                        callback();
                    }
                );
            },
        // Get all devices
            (callback) => {
                rest.post('/v1/devices/get_devices',
                    {},
                    (err, req, res, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 2);

                        callback();
                    }
                );
            },
        // Update the device
            (callback) => {
                device1.description = 'Updated Name 1';

                rest.post('/v1/devices/update_device',
                    { 
                        device: device1
                    },
                    (err, req, res, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.description, 'Updated Name 1');
                        assert.equal(device.id, DEVICE1.id);

                        device1 = device;

                        callback();
                    }
                );
            },
            // Generate request code
            (callback) => {
                rest.post('/v1/devices/generate_request_code',
                    {
                        device_id: device1.id
                    },
                    (err, req, res, result) => {
                        assert.isNull(err);
                        assert.isNotNull(result.code);
                        callback();
                    }
                );
            },
        // Delete device
            (callback) => {
                rest.post('/v1/devices/delete_device_by_id',
                    {
                        device_id: device1.id
                    },
                    (err, req, res, result) => {
                        assert.isNull(err);

                        //assert.isNull(result);

                        callback();
                    }
                );
            },
        // Try to get delete device
            (callback) => {
                rest.post('/v1/devices/get_device_by_id',
                    {
                        device_id: device1.id
                    },
                    (err, req, res, result) => {
                        assert.isNull(err);

                        //assert.isNull(result);

                        callback();
                    }
                );
            }
        ], done);
    });
});