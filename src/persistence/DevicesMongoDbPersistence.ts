let _ = require('lodash');

import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IdentifiableMongoDbPersistence } from 'pip-services3-mongodb-node';

import { DeviceV1 } from '../data/version1/DeviceV1';
import { IDevicesPersistence } from './IDevicesPersistence';

export class DevicesMongoDbPersistence
    extends IdentifiableMongoDbPersistence<DeviceV1, string>
    implements IDevicesPersistence {

    constructor() {
        super('devices');
    }

    private composeFilter(filter: any) {
        filter = filter || new FilterParams();

        let criteria = [];

        let search = filter.getAsNullableString('search');
        if (search != null) {
            let searchRegex = new RegExp(search, "i");
            let searchCriteria = [];
            searchCriteria.push({ id: { $regex: searchRegex } });
            searchCriteria.push({ description: { $regex: searchRegex } });
            searchCriteria.push({ request_code: { $regex: searchRegex } });
            criteria.push({ $or: searchCriteria });
        }

        let id = filter.getAsNullableString('id');
        if (id != null)
            criteria.push({ _id: id });

        let request_code = filter.getAsNullableString('request_code');
        if (request_code != null)
            criteria.push({ request_code: request_code });

        // Filter invitation_codes
        let request_codes = filter.getAsObject('request_codes');
        if (_.isString(request_codes))
        request_codes = request_codes.split(',');
        if (_.isArray(request_codes))
            criteria.push({ request_code: { $in: request_codes } });

        return criteria.length > 0 ? { $and: criteria } : null;
    }

    public getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DeviceV1>) => void): void {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }

}
