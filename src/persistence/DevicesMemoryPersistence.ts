let _ = require('lodash');

import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IdentifiableMemoryPersistence } from 'pip-services3-data-node';
import { TagsProcessor } from 'pip-services3-commons-node';

import { DeviceV1 } from '../data/version1/DeviceV1';
import { IDevicesPersistence } from './IDevicesPersistence';

export class DevicesMemoryPersistence
    extends IdentifiableMemoryPersistence<DeviceV1, string>
    implements IDevicesPersistence {

    constructor() {
        super();
    }

    private matchString(value: string, search: string): boolean {
        if (value == null && search == null)
            return true;
        if (value == null || search == null)
            return false;
        return value.toLowerCase().indexOf(search) >= 0;
    }

    private matchSearch(item: DeviceV1, search: string): boolean {
        search = search.toLowerCase();
        if (this.matchString(item.id, search))
            return true;
        if (this.matchString(item.description, search))
            return true;
        if (this.matchString(item.request_code, search))
            return true;
        return false;
    }

    private composeFilter(filter: FilterParams): any {
        filter = filter || new FilterParams();

        let search = filter.getAsNullableString('search');
        let id = filter.getAsNullableString('id');
        let request_code = filter.getAsNullableString('request_code');
        let request_codes = filter.getAsObject('request_codes');

        // Process request_codes filter
        if (_.isString(request_codes))
            request_codes = request_codes.split(',');
        if (!_.isArray(request_codes))
            request_codes = null;

        return (item) => {
            if (search && !this.matchSearch(item, search))
                return false;
            if (id && item.id != id)
                return false;
            if (request_code && item.request_code != request_code)
                return false;
            if (request_codes && _.indexOf(request_codes, item.invitation_code) < 0)
                return false;

            return true;
        };
    }

    public getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DeviceV1>) => void): void {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }

}
