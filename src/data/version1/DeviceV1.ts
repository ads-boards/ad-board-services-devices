
import { IStringIdentifiable } from 'pip-services3-commons-node';

export class DeviceV1 implements IStringIdentifiable {
    public id: string; // device id
    public request_code: string; // public id for requests from thid devise
    public description: string; // device description
    public last_connection: Date; // time of last request from device
}