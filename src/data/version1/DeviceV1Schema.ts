import { ObjectSchema } from 'pip-services3-commons-node';
import { TypeCode } from 'pip-services3-commons-node';

export class DeviceV1Schema extends ObjectSchema {
    public constructor() {
        super();
        this.withOptionalProperty('id', TypeCode.String);
        this.withRequiredProperty('request_code', TypeCode.String);
        this.withRequiredProperty('description', TypeCode.String);
        this.withOptionalProperty('last_connection', TypeCode.DateTime);
    }
}