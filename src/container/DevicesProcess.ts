
import { ProcessContainer } from 'pip-services3-container-node';

import { DevicesServiceFactory } from '../build/DevicesServiceFactory';
import { DefaultRpcFactory } from 'pip-services3-rpc-node';
import { DefaultGrpcFactory } from 'pip-services3-grpc-node';

export class DevicesProcess extends ProcessContainer {

    public constructor() {
        super("devices-library", "Devices microservice");
        this._factories.add(new DevicesServiceFactory);
        this._factories.add(new DefaultRpcFactory);
        this._factories.add(new DefaultGrpcFactory);
    }

}
