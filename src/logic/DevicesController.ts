
let async = require('async');
import { ConfigParams, NotFoundException } from 'pip-services3-commons-node';
import { IConfigurable } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { IReferenceable } from 'pip-services3-commons-node';
import { DependencyResolver } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { ICommandable } from 'pip-services3-commons-node';
import { CommandSet } from 'pip-services3-commons-node';

import { DeviceV1 } from '../data/version1/DeviceV1';
import { IDevicesPersistence } from '../persistence/IDevicesPersistence';
import { IDevicesController } from './IDevicesController';
import { DevicesCommandSet } from './DevicesCommandSet';

export class DevicesController implements IConfigurable, IReferenceable, ICommandable, IDevicesController {
    private static _defaultConfig: ConfigParams = ConfigParams.fromTuples(
        'dependencies.persistence', 'ad-board-devices:persistence:*:*:1.0'
    );

    private _dependencyResolver: DependencyResolver = new DependencyResolver(DevicesController._defaultConfig);
    private _persistence: IDevicesPersistence;
    private _commandSet: DevicesCommandSet;

    private _textSize: number = 3;
    private _numberSize: number = 3;
    private _codeCount: number = 10;

    public configure(config: ConfigParams): void {
        this._dependencyResolver.configure(config);
    }

    public setReferences(references: IReferences): void {
        this._dependencyResolver.setReferences(references);
        this._persistence = this._dependencyResolver.getOneRequired<IDevicesPersistence>('persistence');
    }

    public getCommandSet(): CommandSet {
        if (this._commandSet == null)
            this._commandSet = new DevicesCommandSet(this);
        return this._commandSet;
    }

    public getDevices(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DeviceV1>) => void): void {
        this._persistence.getPageByFilter(correlationId, filter, paging, callback);
    }

    public getDeviceById(correlationId: string, deviceId: string,
        callback: (err: any, device: DeviceV1) => void): void {
        this._persistence.getOneById(correlationId, deviceId, callback);
    }

    public createDevice(correlationId: string, device: DeviceV1,
        callback: (err: any, item: DeviceV1) => void): void {
        let deviceId: string;
        let item: DeviceV1;
        let code_exist: boolean = false;

        async.series([
            (callback) => {
                this._persistence.create(correlationId, device, (err, device) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    item = device;
                    deviceId = device.id;
                    if (item.request_code != null && item.request_code.length > 0) {
                        code_exist = true;
                    }
                    callback();
                });
            },
            (callback) => {
                if (code_exist) {
                    callback();
                    return;
                }
                this.generateRequstCode(correlationId, deviceId, (err, code) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    callback();
                })
            }
        ], (err) => {
            if (err) {
                callback(err, null);
            } else {
                callback(err, item);
            }
        });
    }

    public updateDevice(correlationId: string, device: DeviceV1,
        callback: (err: any, device: DeviceV1) => void): void {
        this._persistence.update(correlationId, device, callback);
    }

    public deleteDeviceById(correlationId: string, deviceId: string,
        callback: (err: any, device: DeviceV1) => void): void {
        this._persistence.deleteById(correlationId, deviceId, callback);
    }

    public generateRequstCode(correlationId: string, deviceId: string,
        callback: (err: any, code: string) => void): void {

        let device: DeviceV1;
        let request_codes = this.generateRandomRequestCodes(this._textSize, this._numberSize, this._codeCount);
        let request_code = null;
        async.series([
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromTuples('id', deviceId),
                    new PagingParams(0, 1),
                    (err, page) => {
                        if (err != null) {
                            callback(err, null);
                            return;
                        }
                        if (page.data.length == 0) {
                            err = new NotFoundException(
                                correlationId,
                                'NOT_FOUND',
                                'Device ' + deviceId + ' was not found'
                            ).withDetails('id', deviceId);
                            callback(err, null);
                            return;
                        }
                        device = page.data[0];
                        callback();
                    });
            },
            (callback) => {
                let exist_code = false;
                async.whilst(
                    () => { return !exist_code; },
                    (callback) => {
                        this._persistence.getPageByFilter(
                            null,
                            FilterParams.fromTuples('request_code', request_codes),
                            null,
                            (err, page) => {
                                if (err != null) {
                                    exist_code = true;
                                    callback(err, null);
                                    return;
                                }
                                if (page.data.length == 0) {
                                    request_code = request_codes[0];
                                    exist_code = true;
                                    callback();
                                }
                                else {
                                    if (page.data.length == this._codeCount) {
                                        request_codes = this.generateRandomRequestCodes(this._textSize, this._numberSize, this._codeCount);
                                        callback();
                                        return;
                                    }
                                    for (let code of request_codes) {
                                        if (page.data.find(practice => practice.request_code == code))
                                            continue;
                                        request_code = code;
                                        exist_code = true;
                                        break;
                                    }
                                    callback();
                                }
                            });
                    },
                    (err) => {
                        callback(err, null);
                    }
                );
            },
            (callback) => {
                if (request_code == null) {
                    callback();
                    return;
                }
                device.request_code = request_code;
                this._persistence.update(
                    correlationId,
                    device,
                    (err, data) => {
                        device = data;
                        callback(err);
                    }
                );
            }
        ], (err) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, device.request_code);
            }
        });
    }

    private generateRandomRequestCodes(codeTextSize: number, codeNumberSize: number, codeCount: number): string[] {
        let result = [];
        for (let i = 0; i < codeCount; i++) {
            result.push(this.generateRandomRequestCode(codeTextSize, codeNumberSize));
        }

        return result;
    }

    private generateRandomRequestCode(codeTextSize: number, codeNumberSize: number): string {
        let j, temp, arr = Array.from(this.getRandomText(codeTextSize) + this.getRandomNumber(codeNumberSize));
        for (let i = arr.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
        }

        return arr.join("");
    }

    private getRandomText(textSize: number): string {
        let text = "";
        let possibleText = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (let i = 0; i < textSize; i++)
            text += possibleText.charAt(Math.floor(Math.random() * possibleText.length));

        return text;
    }

    private getRandomNumber(numberSize: number): string {
        let number = "";
        let possibleNumber = "0123456789";

        for (let i = 0; i < numberSize; i++)
            number += possibleNumber.charAt(Math.floor(Math.random() * possibleNumber.length));

        return number;
    }

}
