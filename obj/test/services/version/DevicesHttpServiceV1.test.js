"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let _ = require('lodash');
let async = require('async');
let restify = require('restify');
let assert = require('chai').assert;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const DevicesMemoryPersistence_1 = require("../../../src/persistence/DevicesMemoryPersistence");
const DevicesController_1 = require("../../../src/logic/DevicesController");
const DevicesHttpServiceV1_1 = require("../../../src/services/version1/DevicesHttpServiceV1");
let httpConfig = pip_services3_commons_node_1.ConfigParams.fromTuples("connection.protocol", "http", "connection.host", "localhost", "connection.port", 3000);
let DEVICE1 = {
    id: '1',
    request_code: 'id123',
    description: 'Device 1',
    last_connection: new Date()
};
let DEVICE2 = {
    id: '2',
    request_code: '',
    description: 'Device 2',
    last_connection: new Date()
};
suite('DevicesHttpServiceV1', () => {
    let service;
    let rest;
    suiteSetup((done) => {
        let persistence = new DevicesMemoryPersistence_1.DevicesMemoryPersistence();
        let controller = new DevicesController_1.DevicesController();
        service = new DevicesHttpServiceV1_1.DevicesHttpServiceV1();
        service.configure(httpConfig);
        let references = pip_services3_commons_node_3.References.fromTuples(new pip_services3_commons_node_2.Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_2.Descriptor('ad-board-devices', 'controller', 'default', 'default', '1.0'), controller, new pip_services3_commons_node_2.Descriptor('ad-board-devices', 'service', 'http', 'default', '1.0'), service);
        controller.setReferences(references);
        service.setReferences(references);
        service.open(null, done);
    });
    suiteTeardown((done) => {
        service.close(null, done);
    });
    setup(() => {
        let url = 'http://localhost:3000';
        rest = restify.createJsonClient({ url: url, version: '*' });
    });
    test('CRUD Operations', (done) => {
        let device1, device2;
        async.series([
            // Create one device
            (callback) => {
                rest.post('/v1/devices/create_device', {
                    device: DEVICE1
                }, (err, req, res, device) => {
                    assert.isNull(err);
                    assert.isObject(device);
                    assert.equal(device.id, DEVICE1.id);
                    assert.equal(device.request_code, DEVICE1.request_code);
                    assert.equal(device.description, DEVICE1.description);
                    device1 = device;
                    callback();
                });
            },
            // Create another device
            (callback) => {
                rest.post('/v1/devices/create_device', {
                    device: DEVICE2
                }, (err, req, res, device) => {
                    assert.isNull(err);
                    assert.isObject(device);
                    assert.equal(device.id, DEVICE2.id);
                    assert.isNotNull(device.request_code);
                    assert.equal(device.description, DEVICE2.description);
                    device2 = device;
                    callback();
                });
            },
            // Get all devices
            (callback) => {
                rest.post('/v1/devices/get_devices', {}, (err, req, res, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 2);
                    callback();
                });
            },
            // Update the device
            (callback) => {
                device1.description = 'Updated Name 1';
                rest.post('/v1/devices/update_device', {
                    device: device1
                }, (err, req, res, device) => {
                    assert.isNull(err);
                    assert.isObject(device);
                    assert.equal(device.description, 'Updated Name 1');
                    assert.equal(device.id, DEVICE1.id);
                    device1 = device;
                    callback();
                });
            },
            // Generate request code
            (callback) => {
                rest.post('/v1/devices/generate_request_code', {
                    device_id: device1.id
                }, (err, req, res, result) => {
                    assert.isNull(err);
                    assert.isNotNull(result.code);
                    callback();
                });
            },
            // Delete device
            (callback) => {
                rest.post('/v1/devices/delete_device_by_id', {
                    device_id: device1.id
                }, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            },
            // Try to get delete device
            (callback) => {
                rest.post('/v1/devices/get_device_by_id', {
                    device_id: device1.id
                }, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            }
        ], done);
    });
});
//# sourceMappingURL=DevicesHttpServiceV1.test.js.map