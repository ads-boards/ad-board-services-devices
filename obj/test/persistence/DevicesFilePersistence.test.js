"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DevicesFilePersistence_1 = require("../../src/persistence/DevicesFilePersistence");
const DevicesPersistenceFixture_1 = require("./DevicesPersistenceFixture");
suite('DevicesFilePersistence', () => {
    let persistence;
    let fixture;
    setup((done) => {
        persistence = new DevicesFilePersistence_1.DevicesFilePersistence('./data/ads.test.json');
        fixture = new DevicesPersistenceFixture_1.DevicesPersistenceFixture(persistence);
        persistence.open(null, (err) => {
            persistence.clear(null, done);
        });
    });
    teardown((done) => {
        persistence.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });
});
//# sourceMappingURL=DevicesFilePersistence.test.js.map