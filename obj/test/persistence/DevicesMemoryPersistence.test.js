"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const DevicesMemoryPersistence_1 = require("../../src/persistence/DevicesMemoryPersistence");
const DevicesPersistenceFixture_1 = require("./DevicesPersistenceFixture");
suite('DevicesMemoryPersistence', () => {
    let persistence;
    let fixture;
    setup((done) => {
        persistence = new DevicesMemoryPersistence_1.DevicesMemoryPersistence();
        persistence.configure(new pip_services3_commons_node_1.ConfigParams());
        fixture = new DevicesPersistenceFixture_1.DevicesPersistenceFixture(persistence);
        persistence.open(null, done);
    });
    teardown((done) => {
        persistence.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });
});
//# sourceMappingURL=DevicesMemoryPersistence.test.js.map