"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesPersistenceFixture = void 0;
let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
let DEVICE1 = {
    id: '1',
    request_code: 'id123',
    description: 'Device 1',
    last_connection: new Date()
};
let DEVICE2 = {
    id: '2',
    request_code: 'id321',
    description: 'Device 2',
    last_connection: new Date()
};
let DEVICE3 = {
    id: '3',
    request_code: '',
    description: 'Device 3',
    last_connection: new Date()
};
class DevicesPersistenceFixture {
    constructor(persistence) {
        assert.isNotNull(persistence);
        this._persistence = persistence;
    }
    testCreateDevices(done) {
        async.series([
            // Create one device
            (callback) => {
                this._persistence.create(null, DEVICE1, (err, device) => {
                    assert.isNull(err);
                    assert.isObject(device);
                    assert.equal(device.id, DEVICE1.id);
                    assert.equal(device.request_code, DEVICE1.request_code);
                    assert.equal(device.description, DEVICE1.description);
                    callback();
                });
            },
            // Create another device
            (callback) => {
                this._persistence.create(null, DEVICE2, (err, device) => {
                    assert.isNull(err);
                    assert.isObject(device);
                    assert.equal(device.id, DEVICE2.id);
                    assert.equal(device.request_code, DEVICE2.request_code);
                    assert.equal(device.description, DEVICE2.description);
                    callback();
                });
            },
            // Create yet another device
            (callback) => {
                this._persistence.create(null, DEVICE3, (err, device) => {
                    assert.isNull(err);
                    assert.isObject(device);
                    assert.equal(device.id, DEVICE3.id);
                    assert.equal(device.request_code, DEVICE3.request_code);
                    assert.equal(device.description, DEVICE3.description);
                    callback();
                });
            }
        ], done);
    }
    testCrudOperations(done) {
        let device1;
        async.series([
            // Create items
            (callback) => {
                this.testCreateDevices(callback);
            },
            // Get all devices
            (callback) => {
                this._persistence.getPageByFilter(null, new pip_services3_commons_node_1.FilterParams(), new pip_services3_commons_node_2.PagingParams(), (err, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 3);
                    device1 = page.data[0];
                    callback();
                });
            },
            // Update the device
            (callback) => {
                device1.description = 'Updated Text 1';
                this._persistence.update(null, device1, (err, device) => {
                    assert.isNull(err);
                    assert.isObject(device);
                    assert.equal(device.description, 'Updated Text 1');
                    assert.equal(device.id, device1.id);
                    callback();
                });
            },
            // Delete device
            (callback) => {
                this._persistence.deleteById(null, device1.id, (err) => {
                    assert.isNull(err);
                    callback();
                });
            },
            // Try to get delete device
            (callback) => {
                this._persistence.getOneById(null, device1.id, (err, device) => {
                    assert.isNull(err);
                    assert.isNull(device || null);
                    callback();
                });
            }
        ], done);
    }
    testGetWithFilter(done) {
        async.series([
            // Create devices
            (callback) => {
                this.testCreateDevices(callback);
            },
            // Get devices filtered by id
            (callback) => {
                this._persistence.getPageByFilter(null, pip_services3_commons_node_1.FilterParams.fromValue({
                    id: '1'
                }), new pip_services3_commons_node_2.PagingParams(), (err, devices) => {
                    assert.isNull(err);
                    assert.isObject(devices);
                    assert.lengthOf(devices.data, 1);
                    callback();
                });
            },
            // Get devices filtered by search
            (callback) => {
                this._persistence.getPageByFilter(null, pip_services3_commons_node_1.FilterParams.fromValue({
                    search: 'id321'
                }), new pip_services3_commons_node_2.PagingParams(), (err, devices) => {
                    assert.isNull(err);
                    assert.isObject(devices);
                    assert.lengthOf(devices.data, 1);
                    callback();
                });
            }
        ], done);
    }
}
exports.DevicesPersistenceFixture = DevicesPersistenceFixture;
//# sourceMappingURL=DevicesPersistenceFixture.js.map