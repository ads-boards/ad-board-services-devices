import { IStringIdentifiable } from 'pip-services3-commons-node';
export declare class DeviceV1 implements IStringIdentifiable {
    id: string;
    request_code: string;
    description: string;
    last_connection: Date;
}
