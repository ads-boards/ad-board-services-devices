"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeviceV1Schema = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
class DeviceV1Schema extends pip_services3_commons_node_1.ObjectSchema {
    constructor() {
        super();
        this.withOptionalProperty('id', pip_services3_commons_node_2.TypeCode.String);
        this.withRequiredProperty('request_code', pip_services3_commons_node_2.TypeCode.String);
        this.withRequiredProperty('description', pip_services3_commons_node_2.TypeCode.String);
        this.withOptionalProperty('last_connection', pip_services3_commons_node_2.TypeCode.DateTime);
    }
}
exports.DeviceV1Schema = DeviceV1Schema;
//# sourceMappingURL=DeviceV1Schema.js.map