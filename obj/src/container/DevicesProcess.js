"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesProcess = void 0;
const pip_services3_container_node_1 = require("pip-services3-container-node");
const DevicesServiceFactory_1 = require("../build/DevicesServiceFactory");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
const pip_services3_grpc_node_1 = require("pip-services3-grpc-node");
class DevicesProcess extends pip_services3_container_node_1.ProcessContainer {
    constructor() {
        super("devices-library", "Devices microservice");
        this._factories.add(new DevicesServiceFactory_1.DevicesServiceFactory);
        this._factories.add(new pip_services3_rpc_node_1.DefaultRpcFactory);
        this._factories.add(new pip_services3_grpc_node_1.DefaultGrpcFactory);
    }
}
exports.DevicesProcess = DevicesProcess;
//# sourceMappingURL=DevicesProcess.js.map