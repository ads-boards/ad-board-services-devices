"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesMemoryPersistence = void 0;
let _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_data_node_1 = require("pip-services3-data-node");
class DevicesMemoryPersistence extends pip_services3_data_node_1.IdentifiableMemoryPersistence {
    constructor() {
        super();
    }
    matchString(value, search) {
        if (value == null && search == null)
            return true;
        if (value == null || search == null)
            return false;
        return value.toLowerCase().indexOf(search) >= 0;
    }
    matchSearch(item, search) {
        search = search.toLowerCase();
        if (this.matchString(item.id, search))
            return true;
        if (this.matchString(item.description, search))
            return true;
        if (this.matchString(item.request_code, search))
            return true;
        return false;
    }
    composeFilter(filter) {
        filter = filter || new pip_services3_commons_node_1.FilterParams();
        let search = filter.getAsNullableString('search');
        let id = filter.getAsNullableString('id');
        let request_code = filter.getAsNullableString('request_code');
        let request_codes = filter.getAsObject('request_codes');
        // Process request_codes filter
        if (_.isString(request_codes))
            request_codes = request_codes.split(',');
        if (!_.isArray(request_codes))
            request_codes = null;
        return (item) => {
            if (search && !this.matchSearch(item, search))
                return false;
            if (id && item.id != id)
                return false;
            if (request_code && item.request_code != request_code)
                return false;
            if (request_codes && _.indexOf(request_codes, item.invitation_code) < 0)
                return false;
            return true;
        };
    }
    getPageByFilter(correlationId, filter, paging, callback) {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }
}
exports.DevicesMemoryPersistence = DevicesMemoryPersistence;
//# sourceMappingURL=DevicesMemoryPersistence.js.map