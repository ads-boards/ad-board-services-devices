"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesMongoDbPersistence = void 0;
let _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_mongodb_node_1 = require("pip-services3-mongodb-node");
class DevicesMongoDbPersistence extends pip_services3_mongodb_node_1.IdentifiableMongoDbPersistence {
    constructor() {
        super('devices');
    }
    composeFilter(filter) {
        filter = filter || new pip_services3_commons_node_1.FilterParams();
        let criteria = [];
        let search = filter.getAsNullableString('search');
        if (search != null) {
            let searchRegex = new RegExp(search, "i");
            let searchCriteria = [];
            searchCriteria.push({ id: { $regex: searchRegex } });
            searchCriteria.push({ description: { $regex: searchRegex } });
            searchCriteria.push({ request_code: { $regex: searchRegex } });
            criteria.push({ $or: searchCriteria });
        }
        let id = filter.getAsNullableString('id');
        if (id != null)
            criteria.push({ _id: id });
        let request_code = filter.getAsNullableString('request_code');
        if (request_code != null)
            criteria.push({ request_code: request_code });
        // Filter invitation_codes
        let request_codes = filter.getAsObject('request_codes');
        if (_.isString(request_codes))
            request_codes = request_codes.split(',');
        if (_.isArray(request_codes))
            criteria.push({ request_code: { $in: request_codes } });
        return criteria.length > 0 ? { $and: criteria } : null;
    }
    getPageByFilter(correlationId, filter, paging, callback) {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }
}
exports.DevicesMongoDbPersistence = DevicesMongoDbPersistence;
//# sourceMappingURL=DevicesMongoDbPersistence.js.map