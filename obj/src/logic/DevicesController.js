"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesController = void 0;
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const pip_services3_commons_node_4 = require("pip-services3-commons-node");
const DevicesCommandSet_1 = require("./DevicesCommandSet");
class DevicesController {
    constructor() {
        this._dependencyResolver = new pip_services3_commons_node_2.DependencyResolver(DevicesController._defaultConfig);
        this._textSize = 3;
        this._numberSize = 3;
        this._codeCount = 10;
    }
    configure(config) {
        this._dependencyResolver.configure(config);
    }
    setReferences(references) {
        this._dependencyResolver.setReferences(references);
        this._persistence = this._dependencyResolver.getOneRequired('persistence');
    }
    getCommandSet() {
        if (this._commandSet == null)
            this._commandSet = new DevicesCommandSet_1.DevicesCommandSet(this);
        return this._commandSet;
    }
    getDevices(correlationId, filter, paging, callback) {
        this._persistence.getPageByFilter(correlationId, filter, paging, callback);
    }
    getDeviceById(correlationId, deviceId, callback) {
        this._persistence.getOneById(correlationId, deviceId, callback);
    }
    createDevice(correlationId, device, callback) {
        let deviceId;
        let item;
        let code_exist = false;
        async.series([
            (callback) => {
                this._persistence.create(correlationId, device, (err, device) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    item = device;
                    deviceId = device.id;
                    if (item.request_code != null && item.request_code.length > 0) {
                        code_exist = true;
                    }
                    callback();
                });
            },
            (callback) => {
                if (code_exist) {
                    callback();
                    return;
                }
                this.generateRequstCode(correlationId, deviceId, (err, code) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    callback();
                });
            }
        ], (err) => {
            if (err) {
                callback(err, null);
            }
            else {
                callback(err, item);
            }
        });
    }
    updateDevice(correlationId, device, callback) {
        this._persistence.update(correlationId, device, callback);
    }
    deleteDeviceById(correlationId, deviceId, callback) {
        this._persistence.deleteById(correlationId, deviceId, callback);
    }
    generateRequstCode(correlationId, deviceId, callback) {
        let device;
        let request_codes = this.generateRandomRequestCodes(this._textSize, this._numberSize, this._codeCount);
        let request_code = null;
        async.series([
            (callback) => {
                this._persistence.getPageByFilter(null, pip_services3_commons_node_3.FilterParams.fromTuples('id', deviceId), new pip_services3_commons_node_4.PagingParams(0, 1), (err, page) => {
                    if (err != null) {
                        callback(err, null);
                        return;
                    }
                    if (page.data.length == 0) {
                        err = new pip_services3_commons_node_1.NotFoundException(correlationId, 'NOT_FOUND', 'Device ' + deviceId + ' was not found').withDetails('id', deviceId);
                        callback(err, null);
                        return;
                    }
                    device = page.data[0];
                    callback();
                });
            },
            (callback) => {
                let exist_code = false;
                async.whilst(() => { return !exist_code; }, (callback) => {
                    this._persistence.getPageByFilter(null, pip_services3_commons_node_3.FilterParams.fromTuples('request_code', request_codes), null, (err, page) => {
                        if (err != null) {
                            exist_code = true;
                            callback(err, null);
                            return;
                        }
                        if (page.data.length == 0) {
                            request_code = request_codes[0];
                            exist_code = true;
                            callback();
                        }
                        else {
                            if (page.data.length == this._codeCount) {
                                request_codes = this.generateRandomRequestCodes(this._textSize, this._numberSize, this._codeCount);
                                callback();
                                return;
                            }
                            for (let code of request_codes) {
                                if (page.data.find(practice => practice.request_code == code))
                                    continue;
                                request_code = code;
                                exist_code = true;
                                break;
                            }
                            callback();
                        }
                    });
                }, (err) => {
                    callback(err, null);
                });
            },
            (callback) => {
                if (request_code == null) {
                    callback();
                    return;
                }
                device.request_code = request_code;
                this._persistence.update(correlationId, device, (err, data) => {
                    device = data;
                    callback(err);
                });
            }
        ], (err) => {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, device.request_code);
            }
        });
    }
    generateRandomRequestCodes(codeTextSize, codeNumberSize, codeCount) {
        let result = [];
        for (let i = 0; i < codeCount; i++) {
            result.push(this.generateRandomRequestCode(codeTextSize, codeNumberSize));
        }
        return result;
    }
    generateRandomRequestCode(codeTextSize, codeNumberSize) {
        let j, temp, arr = Array.from(this.getRandomText(codeTextSize) + this.getRandomNumber(codeNumberSize));
        for (let i = arr.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
        }
        return arr.join("");
    }
    getRandomText(textSize) {
        let text = "";
        let possibleText = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        for (let i = 0; i < textSize; i++)
            text += possibleText.charAt(Math.floor(Math.random() * possibleText.length));
        return text;
    }
    getRandomNumber(numberSize) {
        let number = "";
        let possibleNumber = "0123456789";
        for (let i = 0; i < numberSize; i++)
            number += possibleNumber.charAt(Math.floor(Math.random() * possibleNumber.length));
        return number;
    }
}
exports.DevicesController = DevicesController;
DevicesController._defaultConfig = pip_services3_commons_node_1.ConfigParams.fromTuples('dependencies.persistence', 'ad-board-devices:persistence:*:*:1.0');
//# sourceMappingURL=DevicesController.js.map