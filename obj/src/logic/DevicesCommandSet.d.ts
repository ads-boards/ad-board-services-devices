import { CommandSet } from 'pip-services3-commons-node';
import { IDevicesController } from './IDevicesController';
export declare class DevicesCommandSet extends CommandSet {
    private _logic;
    constructor(logic: IDevicesController);
    private makeGetDevicesCommand;
    private makeGetDeviceByIdCommand;
    private makeCreateDeviceCommand;
    private makeUpdateDeviceCommand;
    private makeDeleteDeviceByIdCommand;
    private makeGenerateRequestCodeCommand;
}
